# VSCode Dev Container for GitLab Environment Toolkit


This project establishes a [VSCode Dev Container](https://code.visualstudio.com/docs/devcontainers/containers)↗ for running the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit)↗ (GET) on a developer workstation.

## Setup

### Prerequisites

-   [Microsoft Dev Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)↗ installed in VSCode
-   Container provider running (Docker Desktop, [SUSE Rancher Desktop](https://docs.rancherdesktop.io/getting-started/installation#macos)↗ in *Moby* mode, or [Docker Engine for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)↗)

### Installation

1.  Clone this repository into `.devcontainer` in your GET working copy:

    Note: The GET working copy path doesn't have to end with "gitlab-environment-toolkit".

    ```
    cd path/to/gitlab-environment-toolkit
    git clone git@gitlab.com:mlockhart/get-container.git .devcontainer
    # -- or --
    git subrepo clone git@gitlab.com:mlockhart/get-container .devcontainer
    ```

1.  Review the `devcontainer.json` and update the external `mounts` for where you keep your keys, licenses, and environment configurations. Or comment this part out to use the files internally in the GET working directory.

1.  Open the `gitlab-environment-toolkit` folder in VSCode.

1.  When prompted, choose **Reopen in Container**, or use the `Dev Containers: Open Folder in Container...` command from the command palette.

The first time this runs, VSCode will create a new container and provision it using GET's `Dockerfile`. This takes a few minutes, during which there is no update in the VSCode interface. Once the container is ready, VSCode will connect and then the `postCreateCommand` will execute, finishing the setup. The `postCreateCommand` can be re-run if necessary, or you can rebuild the container. Subsequent rebuilds are much faster and have better user experience.

A rebuild is likely necessary if you reconfigure for your own environment. See the next section.

## Configuration

Likely the first run will fail because it is set to my own configuration. You need to check the authentication details are correct and have been set up in GCP properly.

See [GCP authentication and setup instructions](doc/gcp-authentication.md).

The dev container runs with the GCP keys, GitLab licenses, and terraform/ansible environments stored _externally_, and mounts them into place, as [described in the environment provision documentation](docs/environment_provision.md#4-run-the-gitlab-environment-toolkits-docker-container-optional).

## Usage

### Running GET

Run GET as usual and described in the GitLab Environment Toolkit documentation.

-   configure terraform and ansible files in the `TERRAFORM` and `ANSIBLE` folders, which are shortcuts into GET's `terraform` and `ansible` folders.
-   Use `terraform` and `ansible-playbook` from a terminal in the VSCode Dev Container.
-   Authentication to GCP uses settings from `gcloud-auth.sh` and environment variables.

### Configuring GitLab Environment

A motivation for this project is to automate the tedium in configuring GET for creating different supported GitLab environment architectures.

The provided [gcp-environment](/doc/gcp-environment.md) Python script will create a new environment _setup_ (but not actually deploy it), using the [examples](../examples/) from GET, or some [more architecture templates in this project](arch/).

You can read about the script, or an [example steps to configure and deploy a GitLab 2k environment using GET](doc/get-example.md).

### Upgrading GET

To upgrade the toolkit itself, the directions in [GET Upgrades](../docs/environment_upgrades.md) may be simplified to:

1. Review the [Toolkit's release notes](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/releases)↗
1. Chekout the desired GET tag. For example  `cdh; git checkout 3.4.0`
1. Rebuild the container in VSCode with <kbd>F1</kbd> `Dev Containers: Rebuild Container`. This will provision and install updated versions of:
    - [Ansible host python packages](../docs/environment_configure.md#1-install-ansible)
    - [Ansible Galaxy collections & roles](../docs/environment_configure.md#1-install-ansible)
    - [Terraform modules](../docs/environment_provision.md#5-provision)
1. In _each of your Terraform deployments_, upgrade the [Terraform modules](../docs/environment_provision.md#5-provision) - `terraform init --upgrade`
1. Re-apply terraform and ansible to affect changes in the environments for the GET upgrade (not the GitLab upgrade)

### Upgrading GitLab

1.  Update `gitlab_version` in `ansible/environments/XXXX/inventory/vars.yml`.
2.  Run the upgrade playbook:
    ```
    cd $GET_CONT/ANSIBLE
    ansible-playbook -i XXXX/inventory/ playbooks/zero_downtime_update.yml
    ```

    Replace "XXXX" with your environment name.

## Additional Information

For more detailed information, refer to [the official GitLab Environment Toolkit documentation](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/README.md)↗.

### Aliases and functions

There are bash aliases and functions to save your finger mileage typing `terraform deploy` or `ansible-playbook -i environments/${E}/inventory/ playbooks/all.yml;`.  Check them out in [.files/aliases.sh](.files/aliases.sh) and [.files/functions.sh](.files/functions.sh).

### Included Utilities

-   `less` pager
-   `tmux` for managing multiple compute instances
-   `pwgen` for password generation
-   Various other Unix utilities

### Troubleshooting GitLab Upgrades

1.  Check Ansible logs for errors.
2.  Verify GitLab services post-upgrade.
3.  Consult GitLab upgrade documentation for version-specific instructions.

