module "gitlab_ref_arch_gcp" {
  source = "../../modules/gitlab_ref_arch_gcp"

  prefix  = var.prefix
  project = var.project


  # 2k  users / 40 RPS Reference Architecture
  # https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html

  gitaly_node_count = 1
  gitaly_machine_type = "n1-standard-4"

  gitlab_rails_node_count = 2
  gitlab_rails_machine_type = "n1-highcpu-8"

  sidekiq_node_count = 1
  sidekiq_machine_type = "n1-standard-4"

  haproxy_external_node_count        = 1
  haproxy_external_machine_type = "n1-highcpu-4"
  haproxy_external_external_ips = [var.external_ip]

  monitor_node_count = 1
  monitor_machine_type = "n1-highcpu-2"

  postgres_node_count = 1
  postgres_machine_type = "n1-standard-2"

  redis_node_count = 1
  redis_machine_type = "n1-standard-2"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
