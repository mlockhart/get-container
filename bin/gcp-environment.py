#!/usr/bin/env python
# Generate the Terraform and Ansible configurations to build a GitLab environment using GET
# Using the templates in GET's examples folder.

import argparse
import re
import os
import pathlib

GET_HOME = os.environ.get('GET_HOME') \
	or os.environ.get('WORKSPACE', '/workspaces/gitlab-environment-toolkit')

DEFAULTS = {
	'cloud':'gcp',
	'ident':os.environ.get('GCP_IDENT','mjl5658'),
	'project':os.environ.get('GCP_PROJECT','mlockhart-56581c10'),
	'region':os.environ.get('GCP_REGION','us-west1'),
	'zone':os.environ.get('GCP_ZONE','us-west1-c'),
	'bucket':os.environ.get('GCP_TF_STATE_BUCKET','mjl-get-terraform-state'),
	'ssh_username':os.environ.get('GCP_SA_USER','sa_104481838115385290889'),
	'ssh_key_path':os.environ.get('GCP_SA_SSH_PRIV', f"{GET_HOME}/keys/service-account_rsa"),
	'arch':'1k',
	'disk_size':20,
	'password':os.environ.get('GET_ROOT_PASSWORD','ruchae6Zooh'),
	'ansible_environments':f"{GET_HOME}/ansible/environments",
	'terraform_environments':f"{GET_HOME}/terraform/environments",
	'templates':f"{GET_HOME}/examples",
	'license':os.environ.get('GET_LICENSE', f"{GET_HOME}/licenses/gitlab-license-premium"),
	}

def parse_arguments():
	'''Parse command line arguments, return them as a dictionary.

	The defaults for some arguments are set in the global DEFAULTS dictionary.'''

	parser = argparse.ArgumentParser(
		description='Create Terraform and Ansible configurations for deploying GitLab to  GCP with GET.')

	parser.add_argument('-p', '--prefix', help='Name prefix for environment',
		type=str, required=True)
	parser.add_argument('-i', '--external_ip', help='External IP address to use for GitLab',
		type=str, required=True)
	parser.add_argument('-u', '--url', help='GitLab external URL',
		type=str, required=False)
	parser.add_argument('-a', '--arch', help='GitLab Reference Architecture',
		type=str, default='1k',
		choices=['1k', '2k', '3k', '5k', '10k', '25k', '50k'])
	parser.add_argument('-g', '--glver', help='GitLab version to install',
		type=str, required=True)
	parser.add_argument('-r', '--password', help='GitLab root password',
		type=str, default=DEFAULTS['password'])
	parser.add_argument('-L', '--license', help='GitLab license file',
		type=str, default=DEFAULTS['license'])

	parser.add_argument('-E', '--economic', help='Make all Machine types e2-standard-2',
		required=False, action='store_true')
	parser.add_argument('-D', '--disk_size', help='Economic default disk size in GB',
		type=int, default=DEFAULTS['disk_size'])

	parser.add_argument('-I', '--ident', help='GCP Identity prefix for environment',
		type=str, default=DEFAULTS['ident'])
	parser.add_argument('-P', '--project', help='GCP Project ID',
		type=str, default=DEFAULTS['project'])
	parser.add_argument('-R', '--region', help='GCP Region',
		type=str, default=DEFAULTS['region'])
	parser.add_argument('-Z', '--zone', help='GCP Zone',
		type=str, default=DEFAULTS['zone'])
	parser.add_argument('-B', '--bucket', help='GCP Terraform State Bucket Name',
		type=str, default=DEFAULTS['bucket'])

	parser.add_argument('-S', '--ssh_username', help='Ansible SSH Username',
		type=str, default=DEFAULTS['ssh_username'])
	parser.add_argument('-K', '--ssh_key_path', help='Ansible SSH Private Key Path',
		type=str, default=DEFAULTS['ssh_key_path'])
	parser.add_argument('-A', '--ansible', help='Ansible environment directory',
		type=str, default=DEFAULTS['ansible_environments'])
	parser.add_argument('-T', '--terraform', help='Terraform environment directory',
		type=str, default=DEFAULTS['terraform_environments'])
	parser.add_argument('-t', '--templates', help='Templates directory',
		type=str, default=DEFAULTS['templates'])

	return parser.parse_args()

def main():
	args = parse_arguments()

	print (args)

	prefix = path_prefix(args)
	pathlib.Path(f'{args.ansible}/{prefix}/inventory').mkdir(parents=True, exist_ok=True)
	pathlib.Path(f'{args.terraform}/{prefix}').mkdir(parents=True, exist_ok=True)

	generate_terraform_environment(args)
	generate_terraform_main(args)
	generate_terraform_variables(args)
	generate_ansible_inventory(args)
	generate_ansible_variables(args)

def path_prefix(args):
	'''Return the path prefix for the configuration'''
	return f'{args.prefix}_{args.arch}_{translate_version(args.glver)}'

def environment_prefix(args):
	'''Return the environment prefix for the deployment

		For a 1k environment, include the GitLab version and architecture in
		the prefix, to better distinguish multiple 1k deployments.

		For larger environments, only include the short prefix and
		architecture, since I'm unlikely to deploy multiple vesions of these
		at the same time.

		Always start with the ident, to keep resources globally unique.
	'''
	if args.arch == '1k':
		return f'{args.ident}-{args.prefix}-{args.arch}-{translate_version(args.glver)}'
	else:
		return f'{args.ident}-{args.prefix}-{args.arch}'

def translate_version(version_string):
	'''Translate a GitLab version string into a six-digit version string'''
	# Split the version string into parts
	parts = version_string.split('.')
	# Ensure there are exactly three parts, padding with zeros if necessary
	while len(parts) < 3:
		parts.append('0')
	# Convert each part to a two-digit string
	formatted_parts = [part.zfill(2) for part in parts]
	# Combine the parts and add the 'v' prefix
	return 'v' + ''.join(formatted_parts)

def generate_ansible_inventory(args):
	'''Generate Ansible inventory from template'''
	cloud = DEFAULTS['cloud']
	prefix = path_prefix(args)
	inventory_tmpl = f'{args.templates}/{args.arch}_{cloud}/ansible/{args.arch}.{cloud}.yml'
	inventory_file = f'{args.ansible}/{prefix}/inventory/{args.arch}.{cloud}.yml'

	with open(inventory_tmpl, 'r') as file:
		buffer = file.read()

	buffer = buffer.replace('<gcp_project_id>', args.project)
	buffer = buffer.replace('<environment_prefix>', environment_prefix(args))

	with open(inventory_file, 'w') as file:
		file.write(buffer)

	print(f'Generated {inventory_file}')

def generate_ansible_variables(args):
	'''Generate Ansible variables from template'''
	prefix = path_prefix(args)
	variables_tmpl = f'{args.templates}/{args.arch}_{DEFAULTS["cloud"]}/ansible/vars.yml'
	variables_file = f'{args.ansible}/{prefix}/inventory/vars.yml'

	with open(variables_tmpl, 'r') as file:
		buffer = file.read()

	buffer = buffer.replace('<ssh_username>', args.ssh_username)
	buffer = buffer.replace('<private_ssh_key_path>', args.ssh_key_path)
	buffer = buffer.replace('<gcp_project_id>', args.project)
	buffer = buffer.replace('<environment_prefix>', environment_prefix(args))
	buffer = buffer.replace('<external_url>',
		args.url if args.url is not None else f'http://{args.external_ip}')
	buffer = buffer.replace('<gitlab_license_file_path>', args.license)

	# Replace all the _password and _token entries with the password
	pattern = r'(_password):.*'
	buffer = re.sub(pattern, r'\1' + f": '{args.password}'", buffer)
	pattern = r'(_token):.*'
	buffer = re.sub(pattern, r'\1' + f": 'tk:{args.password}'", buffer)

	# Add the gitlab_version. Remove any version copied from the template
	# (I don't think any templates have this, but just in case...)
	buffer = re.sub(r'\n\s*gitlab_version.*\n', '\n', buffer)
	buffer += f"\n    gitlab_version: '{args.glver}'"

	with open(variables_file, 'w') as file:
		file.write(buffer)

	print(f'Generated {variables_file}')

def generate_terraform_environment(args):
	'''Generate Terraform environment file from template'''
	prefix = path_prefix(args)
	terraform_environment_tmpl = f'{args.templates}/{args.arch}_{DEFAULTS["cloud"]}/terraform/environment.tf'
	terraform_environment_file = f'{args.terraform}/{prefix}/environment.tf'

	with open(terraform_environment_tmpl, 'r') as file:
		buffer = file.read()

	# Insert service_account_prefix to avoild clashes with existing
	# service accounts for other deployments. Especially important for the
	# rails nodes. It goes after the 'source = ...' line in the yaml file.

	pattern = r'(source\s*=\s*.*\n)'
	service_acct_line = f'\n  service_account_prefix = "{args.prefix}"\n'
	replacement = r'\1' + service_acct_line
	buffer = re.sub(pattern, replacement, buffer, count=1)

	# Replace gitlab_rails_external_ips = [var.external_ip] with haproxy for the
	# external IP when deploying non-1k + architectures (indent two spaces):

	if args.arch != '1k':
		replacement = '\n'.join([
			'  haproxy_external_node_count = 1',
			'  haproxy_external_machine_type = "n1-highcpu-2"',
			'  haproxy_external_external_ips = [var.external_ip]'
		])
		pattern = r'(gitlab_rails_external_ips\s*=\s*\[.*\]\n)'
		buffer = re.sub(pattern, replacement + '\n\n', buffer, count=1)

	# Replace the machine type with e2-standard-2 and disk_size, to save
	# cost (i.e. we are building a test reference architecture, and don't
	# need the CPU/RAM size for the stated capacity, since only one or a
	# few people will be using it.)
	if args.economic:
		lines = buffer.splitlines()
		new_buffer = []
		for line in lines:
			match = re.search(r'(\w+)_machine_type\s*=\s*.*', line)
			if match:
				captured_word = match.group(1)
				new_line = f"  {captured_word}_machine_type = \"e2-standard-2\""
				new_buffer.append(new_line)
				new_buffer.append(f"  {captured_word}_disk_size = {args.disk_size}")
			else:
				new_buffer.append(line)
		buffer = '\n'.join(new_buffer)

	with open(terraform_environment_file, 'w') as file:
		file.write(buffer)

	print(f'Generated {terraform_environment_file}')

def generate_terraform_main(args):
	'''Generate Terraform main file from template'''
	prefix = path_prefix(args)
	terraform_main_tmpl = f'{args.templates}/{args.arch}_{DEFAULTS["cloud"]}/terraform/main.tf'
	terraform_main_file = f'{args.terraform}/{prefix}/main.tf'

	with open(terraform_main_tmpl, 'r') as file:
		buffer = file.read()

	buffer = buffer.replace('<state_gcp_storage_bucket_name>', args.bucket)
	buffer = buffer.replace('<environment_prefix>', environment_prefix(args))

	with open(terraform_main_file, 'w') as file:
		file.write(buffer)

	print(f'Generated {terraform_main_file}')

def generate_terraform_variables(args):
	'''Generate Terraform variables file from template'''
	prefix = path_prefix(args)
	terraform_variables_tmpl = f'{args.templates}/{args.arch}_{DEFAULTS["cloud"]}/terraform/variables.tf'
	terraform_variables_file = f'{args.terraform}/{prefix}/variables.tf'

	with open(terraform_variables_tmpl, 'r') as file:
		buffer = file.read()

	buffer = buffer.replace('<environment_prefix>', environment_prefix(args))
	buffer = buffer.replace('<gcp_project_id>', args.project)
	buffer = buffer.replace('<gcp_region>', args.region)
	buffer = buffer.replace('<gcp_zone>', args.zone)
	buffer = buffer.replace('<external_ip>', args.external_ip)
	if args.economic:
		buffer += (
			'\nvariable "default_disk_size" {\n' +
			f'  default = "{args.disk_size}"\n' +
			'}\n'
		)

	with open(terraform_variables_file, 'w') as file:
		file.write(buffer)

	print(f'Generated {terraform_variables_file}')

if __name__ == '__main__':
	main()
