#!/usr/bin/env bash
# see https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#4-setup-ssh-authentication-ssh-os-login-for-gcp-service-account
#  Scripted so that the dev container can recreate from the key files on restart.

set -o allexport
  for E in ~/.*.env; do
    test -f "$E" && source "$E"
  done
set +o allexport

echo -e "\n\n🛜  Connecting to the Googleplex\n\n"

env|sort|awk -F '=' '/^GCP|^GET|^GOOGLE/ {print $1 ":\t" $2}'

echo

gcloud config set project ${GCP_PROJECT}
gcloud auth activate-service-account --key-file=${GOOGLE_CREDENTIALS}
gcloud compute os-login ssh-keys add --key-file=${GCP_SA_SSH}

# These aren't necessary, but are nice to have

gcloud config set compute/region ${GCP_REGION}
gcloud config set compute/zone ${GCP_ZONE}

# These are mentioned in the GET docs, but don't need them, or they mess up terraform?
#echo "SERVICE ACCOUNT: sa_$(gcloud iam service-accounts describe gitlab-mjl-mac@mlockhart-56581c10.iam.gserviceaccount.com --format='value(uniqueId)')"
#gcloud config set account mlockhart@gitlab.com

echo
