## Configures Ansible and Terraform using examples as template

A few points about this iteration

- This is for GCP only
- It doesn't set up the [GET/GCP prerequisites](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#google-cloud-platform-gcp). You still have to do that "manually", but only once per GCP project.
- Some defaults are hard-coded for _my_ GCP project. They can be overridden with either environment variables, or with script arguments, see below
- Names the resources with an "IDENT" identity, the default is _my initials_, followed by my GCP project ID
- Uses a "prefix" which is intended to be a _short_ name to distinguish different deployments' resources. It _must be short_ — don't include the architecture or version in the prefix
- The `<environment_prefix>` is calculated as "IDENT-PREFIX-ARCH-GLVERSION" for 1k deployments, and a shorter "IDENT-PREFIX-ARCH" for large deployments
- Specify the GitLab version as `MM.mm.pp`.  Leading zeros are _not_ necessary.
- Has a switch `-E` / `--economic` which makes all `machine_type`s be `e2-standard-2`, and all boot volumes to be `20`GB — a more economical size than the full ref-arch requirement.
- Currently uses the files in GET `examples` directory as templates. Not all architectures are provided for GCP in those examples. You can specify different templates directories for the terraform and ansible files with the `-t` / `--templates` script argument.

### Environment variables

These are not required, the script will fall-back to hard-coded defaults if absent, or you can override with script arguments.

- `GCP_IDENT` a string to make resources globally unique in GCP
- `GCP_PROJECT` a string: `«gcp_username»-«project_id»` e.g. `mlockhart-56581c10`
- `GCP_REGION` the region to deploy into e.g. `us-west1`
- `GPC_ZONE` the zone to deploy into e.g. `us-west1-c`
- `GCP_TF_STATE_BUCKET` the name of the GCP bucket where terraform will keep the state file
- `GCP_SA_USER` the name of the service account user for ansible SSH
- `GCP_SA_SSH_PRIV` the file path to the private SSH key file
- `GET_ROOT_PASSWORD` a password for the GitLab root account
- `GET_LICENSE` the file path to the GitLab license file (premium by default)
- `GET_HOME` is the directory where the GitLab Environment Toolkit can be found

### Arguments

See `gcp-generator.py --help` for a generated command-line help. The _required_ arguments are:

- `-p` / `--prefix` is the short prefix string, e.g. `mjl0` or whatever you like. Keep it short, and unique to each deployment, to avoid name collisions within your GCP project
- `-i` / `--external_ip` is the external IP to use. Create this with `gcloud compute addresses create «name»`
- `-a` / `--arch` is the [GitLab Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/) to deploy. Must exist in the templates/examples. Ref arch's were renamed, GET still uses the older user-count naming "1k,2k..."
- `-g` / `--glver` is the version of GitLab to install. Use GitLab semver version string «MM.mm.pp» e.g. `15.0.1`

### Examples

Generate configurations for an economical 3k deployment of GitLab 16.11.1:

```shell
PFX=gl1; ARCH=3k; VER=16.11.1
gcloud compute addresses create $PFX --region=us-west1
IP=$(gcloud compute addresses list | awk "/^$PFX/{print \$2}")
gcp-environment -p $PFX -i $IP -a $ARCH -g $VER -E
```
```text
Namespace(prefix='gl1', external_ip='35.247.117.44', url=None, arch='3k', glver='16.11.1', password='defaultpasswordchangeme', license='/workspaces/get/licenses/gitlab-premium', economic=True, disk_size=20, ident='mjl5658', project='mlockhart-56581c10', region='us-west1', zone='us-west1-c', bucket='mjl-get-terraform-state', ssh_username='sa_104481838115385290889', ssh_key_path='/workspaces/get/keys/service-account_rsa', ansible='/workspaces/get/ansible/environments', terraform='/workspaces/get/terraform/environments', templates='/workspaces/get/examples')
Generated terraform/environments/gl1_3k_v161101/environment.tf
Generated terraform/environments/gl1_3k_v161101/main.tf
Generated terraform/environments/gl1_3k_v161101/variables.tf
Generated ansible/environments/gl1_3k_v161101/inventory/3k.gcp.yml
Generated ansible/environments/gl1_3k_v161101/inventory/vars.yml
```

### Templates

By default the script will use the [GET examples](../../examples/) as templates for configuring environments. But GET does not include examples of every [GitLab Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/index.html) for GCP.

Additional templates are in the [arch](../arch/) directory. For convenience you can reference this path using the `$GET_ARCH` variable:


```shell
PFX=gl5 ARCH=2k; VER=17.5.1
gcloud compute addresses create $PFX --region=us-west1
IP=$(gcloud compute addresses list | awk "/^$PFX/{print \$2}")
gcp-environment -p $PFX -i $IP -a $ARCH -g $VER -E -t $GET_ARCH
```
```
Namespace(prefix='gl5', external_ip='34.169.19.81', url=None, arch='2k', glver='17.5.1', password='defaultpasswordchangeme', license='/workspaces/get/licenses/gitlab-premium', economic=True, disk_size=20, ident='mjl5658', project='mlockhart-56581c10', region='us-west1', zone='us-west1-c', bucket='mjl-get-terraform-state', ssh_username='sa_104481838115385290889', ssh_key_path='/workspaces/get/keys/service-account_rsa', ansible='/workspaces/get/ansible/environments', terraform='/workspaces/get/terraform/environments', templates='/workspaces/get/.devcontainer/arch')
Generated /workspaces/get/terraform/environments/gl5_2k_v170501/environment.tf
Generated /workspaces/get/terraform/environments/gl5_2k_v170501/main.tf
Generated /workspaces/get/terraform/environments/gl5_2k_v170501/variables.tf
Generated /workspaces/get/ansible/environments/gl5_2k_v170501/inventory/2k.gcp.yml
Generated /workspaces/get/ansible/environments/gl5_2k_v170501/inventory/vars.yml
```
