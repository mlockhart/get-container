# Authenticating GET to your GCP project

1.  Create a GCP project and service account with correct roles as per [GET environment provisioning documentation for GCP](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#configure-authentication-gcp)↗.

    > **GitLab Team members**: Use the [Gitlab Sandbox Cloud Realm](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)↗ for properly tagged and billed GCP projects.


2.  Place your GCP and SSH keys in the `keys/` directory of your GET working directory.

3.  Place your GitLab licenses in the `licences/` directory of your GET working directory

4.  Set the following values in `.devcontainer/get/config.yml`:

  - credentials:
    - `google_credentials_file:` path to your google authentiaction JSON
    - `service_account_key_file:` name of your private SSH key file in GET's `keys` directory
    - `service_account_user:` the GCP service account name

  - gcp:
    - `project:` your GCP project name
    - `region:` the GCP region in which to deploy GitLab using GET
    - `zone:` the GCP zone for deployments
    - `tf_state_bucket:` name of a GCP S3 bucket to store terraform state in

  - secrets:
    - `root_password:` A default password for GitLab deployments

  - licenses:
    - `gitlab-license-premium:` name of your premium license file in GET's `licenses` directory
    - `gitlab-license-ultimate:` name of your ultimate license file in GET's `licenses` directory

### Additional Configuration

-   To disable Milohax personal dotfiles and bash library "Bashing", set `install_radix: false` in `get/config.yml`.
