# GET Deployment example

Here is a short run-down of steps to create an environment using the GitLab Environment Toolkit, configured with [gcp-environment](gcp-environment.md) and some aliases and functions in the GET container.

 In this example, the prefix is `gl1`, and I provision a "2k_gcp" reference architecture from the devcontainer's `arch` templates, scaled for ecconomic CPU/disc, with GitLab version 17.4.1.

This is all done from VSCode's built-in terminal in the VSCode Dev Container.

## 1) Make an IP address:

You can do this with a `gcloud` [alias](.files/aliases.sh):  `gaddress create gl1`

## 2) Generate config:

Use the provided [gcp-environment](gcp-environment.md) Python script to create a new environment setup, with these arguments — the rest are default values:

```shell
gcp-environment -p gl1 -i $(gaddress-ip gl1) -a 2k -g 17.4.1 -t $GET_ARCH -E -r $(pwgen 16)
```

If using one of the GET `examples` as a template, then the `-t $GET_ARCH` is unnecessary.

At this stage, the new environment is ready to be provisioned and configured. You may want to review the configuration files if you will be doing advanced setups, or configuring GitLab Geo.

## 3) Provision the infrastructure

These steps use [shell aliases](.files/aliases.sh) to save typing:

1. change to the **t**erraform/**e**nvironments directory: `cdte`
1. change into the created environment config (using <kbd>TAB</kbd>): `cd gl1_2k_v170401/`
1. initialise terraform and do a timed, approved deployment: `tf init; tfap`

## 4) Configure GitLab

These steps use [shell aliases](.files/aliases.sh) and [functions](.files/functions.sh) to save typing:

1. change to the **a**nsible/**e**nvironments directory: `cdae`
1. run a timed ansible playbook for a deployment (using <kbd>TAB</kbd> for the environment name):

    `aplayall gl1_2k_v170401/`

## 5) Set up server SSH keys and aliases

While Ansible is running, you can perform this step from your container's host computer. I have [a ruby script to manage GCP nodes](https://gitlab.com/mlockhart/lab/-/blob/master/googleplex/glplex)↗, which can greatly simplify this:


```shell
glplex keys gl1
glplex ssh -s gl1 > ~/.ssh/conf.d/gl1.ssh
```

(and make sure your `~/.ssh/config` contains the line `Include conf.d/*`)

This allows direct SSH into the new hosts, using your SSH keys from gitlab.com. No other special tricks are needed, just have your SSH agent running and the keys loaded (I am using 1password):

```shell
ssh mjl5658-gl1-2k-gitlab-rails-1
```

For details, see [my glplex script](https://gitlab.com/mlockhart/lab/-/blob/master/googleplex/glplex)↗. If you can [share your SSH agent to the container](https://code.visualstudio.com/remote/advancedcontainers/sharing-git-credentials)↗, then you may also run it in the container, after installing the script (I can't share the 1password agent to the container).

## 6) Login to the new GitLab deployment

Once GitLab is finished installing and configuring, you can login using the GitLab `root` account. The password will be as was specified with the `-r` switch to `gcp-environment`. You can find this, and the URL in the Ansible inventory for the deployment:

```shell
code gl1_2k_v170401/inventory/vars.yml
```

or extract the details with `gitlab-url`:

```shell
cdae
gitlab-url gl1_2k_v170401/
```
```
http://34.127.19.111
defaultpasswordchangeme
```

(In VSCode's terminal, you can double-click the password to select it, and then copy to the clipboard with <kbd>⌘</kbd>+<kbd>C</kbd>. Then <kbd>⌘</kbd>+_click_ on the URL above it, to open in your web browser)
