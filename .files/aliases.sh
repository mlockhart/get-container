# source for alias definitions

alias q=exit
alias x=exit

# GET aliases
alias tf='time terraform'
alias tfp='terraform plan'
alias tfa='time terraform apply'
alias tfap='time terraform apply --auto-approve=true'
alias ap='ansible-playbook'
alias a='ansible'
alias cdt='cd ${GET_HOME}/terraform/'
alias cda='cd ${GET_HOME}/ansible/'
alias cdk='cd ${GET_HOME}/keys/'
alias cdte='cd ${GET_HOME}/terraform/environments/'
alias cdae='cd ${GET_HOME}/ansible/environments/'
alias cdh='cd ${GET_HOME}'

# VSCode aliases
alias c=code
alias e=code
alias ed=code
alias edit=code
alias cdiff='code --diff'
alias cmerge='code --merge'
export EDITOR='code --wait'

# GCloud aliases
alias gci='gcloud compute instances'
alias gsh='gcloud compute ssh'
alias gauth='gcloud auth activate-service-account --key-file='
alias gacct='gcloud config set account'
alias gaddress='gcloud compute addresses'
alias gcloudauth='${GET_HOME}/.devcontainer/gcloud-auth.sh'
