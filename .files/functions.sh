# source this file for function definitions

# GET functions

error(){
  # This is used by the functions below, but will be replaced by
  # error() from https://gitlab.com/milohax-net/radix/bashing, if that
  # is installed.
  local FUNCDESC="Print arguments to STDERR"
  echo ${@} 1>&2
}

genv(){
  local FUNCDESC='Print GCP/GET/GOOGLE environment variables'
  env|sort|awk -F '=' '/^GCP|^GET|^GOOGLE/ {print $1 ":\t" $2}'
}

aping(){
  local FUNCDESC='Ansible ping all hosts in specified environment'
  [[ -z ${1} ]] && {
    error "${FUNCNAME}: error: must specify an environment from ${GET_HOME}/ansible/environments/"
    return 1
  }
  E=$(echo "${1}"|tr -d '/')
  time ansible all -m ping \
    -i ${GET_HOME}/ansible/environments/${E}/inventory --list-hosts
}

aplayall(){
  local FUNCDESC='Play all ansible playbooks for specified environment'
  [[ -z ${1} ]] && {
    error "${FUNCNAME}: error: must specify an environment from ${GET_HOME}/ansible/environments/"
    return 1
  }
  E=$(echo "${1}"|tr -d '/')
  pushd ${GET_HOME}/ansible/
    time ansible-playbook \
      -i environments/${E}/inventory/ playbooks/all.yml
  popd
}

aplay(){
  local FUNCDESC='Play an ansible playbook, for a specified environment'
  [[ -z ${2} ]] && {
    error "${FUNCNAME}: error: must specify an environment from ${GET_HOME}/ansible/environments/"
    error "and a playbook from ${GET_HOME}/anssible/playbooks"
    echo Usage: ${FUNCNAME} "<environment> <playbook>"
    return 1
  }
  E=$(echo "${1}"|tr -d '/')
  pushd ${GET_HOME}=ansible/
    time ansible-playbook \
      -i environments/${E}/inventory playbooks/${2}
  popd
}

grenv (){
  local FUNCDESC='Print variable names and value for matching regex';
  if [[ -z ${1} ]]; then
    echo "Usage "${FUNCNAME} «variable_name»" ${FUNCDESC};"
    return 1;
  fi;
  env | egrep --color=auto -i  ${1}
}

gaddress-ip (){
  local FUNCDESC='Print the IP addresses for matching GCP server names'
  if [[ -z ${1} ]]; then
    echo "Usage: ${FUNCNAME} «name_pattern»" ${FUNCDESC}
      return 1;
  fi;
  gcloud compute addresses list --filter="name~'${1}'" --format="value(address)"
}

gitlab-url(){
  local FUNCDESC='Print the URL and default root password for a GitLab instance'
  if [[ -z ${1} ]]; then
    echo "Usage: ${FUNCDESC} «name_pattern»" ${FUNCDESC}
    return 1;
  fi;
  E=$(echo "${1}"|tr -d '/')
  yq '.all.vars.external_url, .all.vars.gitlab_root_password' \
    ${GET_CONT}/ANSIBLE/${E}/inventory/vars.yml
}
